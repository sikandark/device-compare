//import 'dart:async';
import 'dart:convert';
import 'dart:io';

Future getAndroidMatchList() async {
  Map<String, dynamic>  deviceFarmJson;
  Map<String, dynamic>  whiteListJson;

  await File('lib/deviceFarmList-android.json').readAsString().then((String contents) {
    deviceFarmJson = json.decode(contents);
  });

  await File('lib/whitelist-android.json').readAsString().then((String contents) {
    whiteListJson = json.decode(contents);
  });

  //get WHITELIST Items from the JSON
  Map<String, dynamic> whiteListJsonItr = whiteListJson.values.elementAt(0).values.elementAt(1);//WHITELIST
  List<dynamic> deviceFarmJsonList = deviceFarmJson.values.elementAt(0);
  var counter = 0;

  print("MATCH ANDROID DEVICE LIST");
  print("=================================================================");
  print('');

  //whiteListJsonItr.forEach((k, v){
    deviceFarmJsonList.forEach((element){
      whiteListJsonItr.forEach((k, v){
        //deviceFarmJsonList.forEach((element){
          if(k.toLowerCase() == element["modelId"].toString().toLowerCase()){
            stdout.write(k);
            stdout.write(' -- ');
            stdout.write(v);
            print('');
            counter++;
          }
        //});
      });
    });
  //});

  stdout.write('Total models available in whiteList- ');
  stdout.write(whiteListJsonItr.length);
  stdout.write('\n');

  stdout.write('Total models available in device farm- ');
  stdout.write(deviceFarmJsonList.length);
  stdout.write('\n');

  stdout.write('Total models Matched- ');
  stdout.write(counter);
  stdout.write('\n');
}
