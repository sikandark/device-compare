//import 'dart:async';
import 'dart:convert';
import 'dart:io';

Future getIOSMatchList() async {
  Map<String, dynamic>  deviceFarmJson;
  Map<String, dynamic>  whiteListJson;

  await File('lib/deviceFarmList-iOS.json').readAsString().then((String contents) {
    deviceFarmJson = json.decode(contents);
  });

  await File('lib/whitelist-iOS.json').readAsString().then((String contents) {
    whiteListJson = json.decode(contents);
  });

  var counter = 0;
  List<dynamic> deviceFarmJsonList = deviceFarmJson.values.elementAt(0);

  List<dynamic> supportedOS = whiteListJson['appVersion_4.1.0']['supported']['os'];
  print('');
  print("MATCH IOS DEVICE LIST");
  print("======================================================");
  print('');
  deviceFarmJsonList.forEach((element){
    if(supportedOS.contains(element['os'])){
      print(element['name'] + " - " + element['os'] + ", modelId - "+ element['modelId']);
      counter++;
    };
  });


  stdout.write('Total models available in device farm- ');
  stdout.write(deviceFarmJsonList.length);
  stdout.write('\n');

  stdout.write('Total models Matched- ');
  stdout.write(counter);
  stdout.write('\n');
}
