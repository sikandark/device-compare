#! /bin/bash
aws devicefarm list-devices --filters attribute=PLATFORM,operator=EQUALS,values=ANDROID > lib/deviceFarmList-android.json
echo "Device Farm Android List generated..."
aws devicefarm list-devices --filters attribute=PLATFORM,operator=EQUALS,values=IOS > lib/deviceFarmList-iOS.json
echo "Device Farm iOS List generated..."